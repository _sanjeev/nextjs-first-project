const Footer = () => {
    return (
        <div className="mt-10">
            <p className="p-5 bg-gray-500 text-center text-white">Copyright @ 2021, Ninjas</p>
        </div>
    );
}

export default Footer;
import Link from "next/link";
import Head from "next/head";

const Navbar = () => {
    return (
        <div className="flex justify-between p-5">
            <div>
                <img src="/logo.png" alt="Logo" className="w-24"/>
            </div>
            <div className="space-x-3 py-6">
                <Link href='/'><a className='hover:underline'>Home</a></Link>
                <Link href='/about'><a className='hover:underline'>About</a></Link>
                <Link href='/contactus'><a className='hover:underline'>Contact Us</a></Link>
                <Link href='/ninjas'><a className='hover:underline'>Ninjas Listing</a></Link>
            </div>
        </div>
    );
}

export default Navbar;
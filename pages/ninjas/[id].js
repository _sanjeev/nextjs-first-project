export const getStaticPaths = async () => {
  const response = await fetch("https://jsonplaceholder.typicode.com/users");
  const data = await response.json();

  const paths = data.map((key) => {
    return {
      params: { id: key.id.toString() },
    };
  });
  return {
    paths: paths,
    fallback: false,
  };
};

export const getStaticProps = async (context) => {
  const id = context.params.id;
  const response = await fetch(
    "https://jsonplaceholder.typicode.com/users/" + id
  );
  const data = await response.json();
  return {
    props: {
      ninjas: data,
    },
  };
};

const Details = ({ ninjas }) => {
  console.log(ninjas);
  return (
    <div className="p-5">
      <h1 className="font-medium text-center text-3xl">User Details</h1>
      <div className="flex flex-col items-center">
        <img src="https://picsum.photos/200" alt="Profile" className="w-[50%] mt-4 mb-4 rounded-3xl"/>
        <p className="text-3xl">{ninjas.name}</p>
        <p className="text-3xl">{ninjas.email}</p>
        <p className="text-3xl">{ninjas.phone}</p>
        <p className="text-3xl">{ninjas.website}</p>
        <p className="text-3xl">{ninjas.address.street}</p>
        <p className="text-3xl">{ninjas.address.suite}</p>
        <p className="text-3xl">{ninjas.address.city}</p>
        <p className="text-3xl">{ninjas.address.zipcode}</p>
      </div>
    </div>
  );
};

export default Details;

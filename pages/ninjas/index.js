export const getStaticProps = async () => {
  const response = await fetch("https://jsonplaceholder.typicode.com/users");
  const data = await response.json();
  return {
    props: { ninjas: data },
  };
};

import Head from "next/head";
import Link from "next/link";

const List = ({ ninjas }) => {
  return (
    <>
      <Head>
        <title>Ninja List | List</title>
        <meta name="keywords" content="ninjas" />
      </Head>
      <div className="p-5">
        <h1 className="text-center text-3xl">All Ninjas</h1>
        {ninjas.map((key) => (
          <Link href={'/ninjas/' + key.id} key={key.id}>
            <div className="flex items-center w-full rounded overflow-hidden shadow-lg mb-2 mt-1 hover:border-l-4 hover:border-blue-600 cursor-pointer">
              <div>
                <img
                  src="https://picsum.photos/200/300"
                  alt="random Image"
                  className="w-[55px] h-[55px] rounded-full"
                />
              </div>
              <div className="pl-4">
                <h1 className="text-2xl font-normal">{key.username}</h1>
                <h2>{key.email}</h2>
              </div>
            </div>
          </Link>
        ))}
      </div>
    </>
  );
};

export default List;

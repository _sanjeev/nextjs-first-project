import Link from "next/link";
import { useEffect } from "react";
import { useRouter } from "next/router";
import Head from "next/head";

const NotFound = () => {
  const router = useRouter();
  useEffect(() => {
    console.log("use effect run");
    setTimeout(() => {
      // router.go(1);
      router.push("/");
    }, 3 * 1000);
  }, []);

  return (
    <>
      <Head>
        <title>Ninja List | 404</title>
        <meta name="keywords" content="ninjas" />
      </Head>
      <div className="p-5 flex flex-col justify-center items-center space-y-4">
        <h1 className="text-3xl font-medium">Ooooops.....</h1>
        <h2 className="text-3xl font-medium">Page Not Found</h2>
        <p>
          Go back to the{" "}
          <Link href="/">
            <a className="hover:underline text-blue-500">HomePage</a>
          </Link>
        </p>
      </div>
    </>
  );
};

export default NotFound;
